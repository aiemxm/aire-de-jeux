window.onload = function () {
        var valid = document.querySelector("button#valider");
        var reset = document.querySelector("button[type=reset]");
        valid.addEventListener('click', function () {
            Contact.setFormValid(true);
            var nom = document.getElementById('nom');
            var prenom = document.getElementById('prenom');
            var tel = document.getElementById('tel');
            var email = document.getElementById('email');

            Contact.setNom(nom);
            Contact.setPrenom(prenom);
            Contact.setTel(tel);
            Contact.setEmail(email);
            Contact.setMessage(message);
            if (Contact.getFormValid()) {
                document.querySelector("div.container>div.row>div:nth-of-type(2)").innerHTML = Contact.afficher();
            }

        })
        reset.addEventListener('click', function () {
            document.querySelector("div.container>div.row>div:nth-of-type(2)").innerHTML = "";

        })
    }
