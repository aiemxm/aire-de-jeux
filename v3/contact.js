var Contact = {
  nom: "",
  prenom: "",
  tel: "",
  email: "",
  message: "",
  formValid: true,

  setNom: function (v) {
    if (this.isAlpha(v.value)) {
      this.nom = v.value;
      this.notError(v);
    } else {
      this.formValid = false;
      this.error(v);
    }
  },
  getNom: function () {
    return this.nom;
  },
  setPrenom: function (v) {
    if (this.isAlpha(v.value)) {
      this.prenom = v.value;
      this.notError(v);
    } else {
      this.formValid = false;
      this.error(v);
    }
  },
  getPrenom: function () {
    return this.prenom;
  },

  setTel: function (v) {
    if (this.isNumerique(v.value)) {
      this.tel = v.value;
      this.notError(v);
    } else {
      this.formValid = false;
      this.error(v);
    }
  },
  getTel: function () {
    return this.tel;
  },

  setEmail: function (v) {
    if (this.isEmail(v.value)) {
      this.email = v.value;
      this.notError(v);
    } else {
      this.formValid = false;
      this.error(v);
    }
  },
  getEmail: function () {
    return this.email;
  },
  getMessage: function () {
    return this.message;
  },
  setMessage: function (v) {
    if (v.value != "") {
      this.message = v.value;
      this.notError(v);
    } else {
      this.formValid = false;
      this.error(v);
    }
  },

  setFormValid: function (b) {
    this.formValid = b;
  },

  getFormValid: function () {
    return this.formValid;
  },

  isAlpha: function (val) {
    var ok = false;
    if (val != "") {
      var regex = /^[a-zA-Z\-]*$/;
      ok = regex.test(val);
    }
    return ok;
  },

  isNumerique: function (val) {
    var ok = false;
    if (val != "") {
      var regex = /^[0-9\.]*$/;
      ok = regex.test(val);
    }
    return ok;
  },

  isEmail: function (val) {
    var ok = false;
    if (val != "") {
      var regex = /^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$/i;
      ok = regex.test(val);
    }
    return ok;
  },
  error: function (element) {
    element.parentNode.parentNode.classList.remove("has-success", "has-error");
    element.parentNode.parentNode.classList.add("has-error");
  },

  notError: function (element) {
    element.parentNode.parentNode.classList.remove("has-success", "has-error");
    element.parentNode.parentNode.classList.add("has-success");
  },

  afficher: function () {
    return (
      this.nom +
      " " +
      this.prenom +
      "<br/>" +
      this.tel +
      "<br/>" +
      this.email +
      "<br/>" +
      this.message
    );
  },
};
